<?php
    session_start();
    $username = $_SESSION['USERNAME'];
	
	$serverName = "eration.database.windows.net";
	$connectionOptions = array(
		"Database" => "Eration",
		"Uid" => "saran",
		"PWD" => "Slytherin73031"
	);
	$conn = sqlsrv_connect($serverName, $connectionOptions);
	$tsql = "SELECT * FROM VENDOR WHERE VNAME='$username'";
	$result = sqlsrv_query($conn, $tsql);
	$r1 = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC);
	$_SESSION['SHOP_ID'] = $r1['SID'];
?>

<html>
<head>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="vendor_home.css">
  <script>
    var transactionID;
	var lastTrans;
	var shopID = "<?php echo $_SESSION['SHOP_ID']; ?>";
	
	function fetchData(){
		$.ajax({
			type: "GET",
			url: "last_transaction.php",
			data: {shop : shopID},
			success: function(data){
				data = JSON.parse(data);
				console.log(data);
				if(lastTrans != data){
					window.location.href = "vendor.php";
				}
				else{
					setTimeout(fetchData, 5000);
				}
			}
		});
	}
	
	$.ajax({
		type: "GET",
		url: "last_transaction.php",
		data: {shop : shopID},
		success: function(data){
			console.log(data);
			lastTrans = JSON.parse(data);
			console.log(lastTrans);
			console.log('started');
			setTimeout(fetchData, 5000);
		}
	});
	
	function confirmOrder(id){
		transactionID = id;
		var myModal = $('#confirmation');
		myModal.modal({ show: true });
		var table = document.getElementById('order');
		var entry;
		$.ajax({
			type: "GET",
			url: "transaction_query.php",
			data: {transaction : id},
			success: function(data){
				console.log(data);
				data = JSON.parse(data);
				table.innerHTML = "";
				for(var i = 0; i < data.length; i++){
					entry = table.insertRow(-1);
					entry.insertCell(0).innerHTML = data[i][0];
					if(data[i][0] == 'Kerosene' || data[i][0] == 'Palm Oil')
						entry.insertCell(1).innerHTML = data[i][1] + ' Ltrs';
					else
						entry.insertCell(1).innerHTML = data[i][1] + ' Kgs';
				}
			}
		});
	}
	function yetToDeliverOrder(id){
		transactionID = id;
		var myModal = $('#yetToDeliver');
		myModal.modal({ show: true });
		var table = document.getElementById('Yorder');
		var entry;
		$.ajax({
			type: "GET",
			url: "transaction_query.php",
			data: {transaction : id},
			success: function(data){
				console.log(data);
				data = JSON.parse(data);
				table.innerHTML = "";
				for(var i = 0; i < data.length; i++){
					entry = table.insertRow(-1);
					entry.insertCell(0).innerHTML = data[i][0];
					if(data[i][0] == 'Kerosene' || data[i][0] == 'Palm Oil')
						entry.insertCell(1).innerHTML = data[i][1] + ' Ltrs';
					else
						entry.insertCell(1).innerHTML = data[i][1] + ' Kgs';
				}
			}
		});
	}
	function viewOrder(id){
		transactionID = id;
		var myModal = $('#doneOrders');
		myModal.modal({ show: true });
		var table = document.getElementById('Dorder');
		var entry;
		$.ajax({
			type: "GET",
			url: "transaction_query.php",
			data: {transaction : id},
			success: function(data){
				console.log(data);
				data = JSON.parse(data);
				table.innerHTML = "";
				for(var i = 0; i < data.length; i++){
					entry = table.insertRow(-1);
					entry.insertCell(0).innerHTML = data[i][0];
					if(data[i][0] == 'Kerosene' || data[i][0] == 'Palm Oil')
						entry.insertCell(1).innerHTML = data[i][1] + ' Ltrs';
					else
						entry.insertCell(1).innerHTML = data[i][1] + ' Kgs';
				}
			}
		});
	}
	function confirmation(){
		$.ajax({
			type: "GET",
			url: "confirm.php",
			data: {transaction : transactionID},
			success: function(data){
				console.log('success');
				window.location.href = "vendor.php";
			}
		});
	}
	function handOver(){
		$.ajax({
			type: "GET",
			url: "handover.php",
			data: {transaction : transactionID},
			success: function(data){
				console.log('success');
				window.location.href = "vendor.php";
			}
		});
	}
  </script>	
</head>
<body>
<nav class="navbar navbar-inverse navbar-height">
	<div class="container-fluid">
    <div class="navbar-header text-center">
      <a class="navbar-brand" href="#"><font face="WildWest" size="6" color="#b37700">E-Ration</font></a>
    </div>
	<ul class="nav navbar-nav navbar-right">
	  <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-user"></span>
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
			<li><a href="#"><b>Hi, <?php echo $username; ?></b></a></li>
			<li class="divider"></li>
			<li><a href="vendor.php">Home</a></li>
			<li><a href="vendor_profile.php">Profile</a></li>
			<li><a href="update_stock.php">Update Stock</a></li>
			<li><a href="index.html">Logout</a></li>
		</ul>
      </li>
    </ul>
	</div>
</nav>

<div class="col-sm-1" style="width:100%">
	<h3 class="table-header"> <span style="background-color:rgba(0,0,0,0.5); padding:2px 8px 10px 8px">New Orders </span></h3>
</div>
<div class="col-sm-1 pending" align="center">
<div class="adjust">
<table class="table" align="center">
    <thead>
      <tr>
        <th>Transaction ID</th>
        <th>Customer Name</th>
        <th>Date</th>
		<th>Status</th>
      </tr>
    </thead>
    <tbody>
	<?php 
		$tsql = "SELECT * FROM TRANSACTIONS  WHERE STATUS='PENDING' ORDER BY ID DESC";
		$getResults = sqlsrv_query($conn, $tsql);
		$hasRow = sqlsrv_has_rows($getResults);
   
		if ($hasRow === false){ ?>
			<tr> <td colspan = "4" style = "text-align:center;"> NO ORDERS TO DISPLAY </td> </tr>
			
	<?php }
		while($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)){?>
			<tr>
				<td class="link" onclick='confirmOrder(<?php echo $row['ID'] ?>);'><u><?php echo $row['ID'] ?></u></td>
				<td><?php echo $row['UNAME'] ?></td>
				<td><?php echo date_format($row['DOT'], 'd M, Y'); ?></td>
				<td><?php echo $row['STATUS'] ?></td>
			</tr>
		<?php } ?>
    </tbody>
  </table>
</div>
</div>

<div class="col-sm-1" style="width:100%">
	<h3 class="table-header"> <span style="background-color:rgba(0,0,0,0.5); padding:2px 8px 10px 8px"> Yet to collect </span></h3>
</div>
<div class="col-sm-1 pending" align="center">
<div class="adjust">
<table class="table" align="center">
    <thead>
      <tr>
        <th>Transaction ID</th>
        <th>Customer Name</th>
        <th>Date</th>
		<th>Status</th>
      </tr>
    </thead>
    <tbody>
	<?php 
		$tsql = "SELECT * FROM TRANSACTIONS WHERE STATUS='CONFIRMED' ORDER BY ID DESC";
		$getResults = sqlsrv_query($conn, $tsql);
		$hasRow = sqlsrv_has_rows($getResults);
   
		if ($hasRow === false){ ?>
			<tr> <td colspan = "4" style = "text-align:center;"> NO ORDERS TO DISPLAY </td> </tr>
			
	<?php }
	
		while($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)){?>
			<tr>
				<td class="link" onclick='yetToDeliverOrder(<?php echo $row['ID'] ?>);'><u style=""><?php echo $row['ID'] ?></u></td>
				<td><?php echo $row['UNAME'] ?></td>
				<td><?php echo date_format($row['DOT'], 'd M, Y'); ?></td>
				<td><?php echo $row['STATUS'] ?></td>
			</tr>
		<?php } ?>
    </tbody>
  </table>
</div>
</div>

<div class="col-sm-1" style="width:100%">
	<h3 class="table-header"> <span style="background-color:rgba(0,0,0,0.5); padding:2px 8px 10px 8px"> Completed Orders </span></h3>
</div>
<div class="col-sm-1 pending" align="center">
<div class="adjust">
<table class="table" align="center">
    <thead>
      <tr>
        <th>Transaction ID</th>
        <th>Customer Name</th>
        <th>Date</th>
		<th>Status</th>
      </tr>
    </thead>
    <tbody>
	<?php 
		$tsql = "SELECT * FROM TRANSACTIONS WHERE STATUS='COLLECTED' ORDER BY ID DESC";
		$getResults = sqlsrv_query($conn, $tsql);
		$hasRow = sqlsrv_has_rows($getResults);
   
		if ($hasRow === false){ ?>
			<tr> <td colspan = "4" style = "text-align:center;"> NO ORDERS TO DISPLAY </td> </tr>
			
	<?php }
	
		while($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)){?>
			<tr>
				<td class="link" onclick='viewOrder(<?php echo $row['ID'] ?>);'><u style=""><?php echo $row['ID'] ?></u></td>
				<td><?php echo $row['UNAME'] ?></td>
				<td><?php echo date_format($row['DOT'], 'd M, Y'); ?></td>
				<td><?php echo $row['STATUS'] ?></td>
			</tr>
		<?php } ?>
    </tbody>
  </table>
</div>
</div>


<div class="modal fade" id="confirmation" role="dialog">
    <div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form>
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title" style="color:black">Order Preview</h4>
				</div>
				<div class="modal-body">
						<table class="table table-bordered" style="text-align:center">
						<thead>
						<tr>
							<th style="color:black; text-align:center">Product</th>
							<th style="color:black; text-align:center">Quantity</th>
						</tr>
						</thead>
						<tbody id="order" style="color:black">
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-success" data-dismiss="modal" onclick="confirmation()";>Confirm</button>
				  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="yetToDeliver" role="dialog">
    <div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form>
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title" style="color:black">Order Preview</h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered" style="text-align:center">
						<thead>
						<tr>
							<th style="color:black; text-align:center">Product</th>
							<th style="color:black; text-align:center">Quantity</th>
						</tr>
						</thead>
						<tbody id="Yorder" style="color:black">
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-success" data-dismiss="modal" onclick='handOver()'>Handover</button>
				  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="doneOrders" role="dialog">
    <div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form>
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title" style="color:black">Order Preview</h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered" style="text-align:center">
						<thead>
						<tr>
							<th style="color:black; text-align:center">Product</th>
							<th style="color:black; text-align:center">Quantity</th>
						</tr>
						</thead>
						<tbody id="Dorder" style="color:black">
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>
