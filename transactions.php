<?php
    session_start();
    $username = $_SESSION['USERNAME'];
	
	$serverName = "eration.database.windows.net";
	$connectionOptions = array(
		"Database" => "Eration",
		"Uid" => "saran",
		"PWD" => "Slytherin73031"
	);
	$conn = sqlsrv_connect($serverName, $connectionOptions);
?>

<html>
<head>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="transactions.css">
  <script>
	function viewOrder(id){
		var myModal = $('#confirmation');
		myModal.modal({ show: true });
		var table = document.getElementById('order');
		var entry;
		$.ajax({
			type: "GET",
			url: "transaction_query.php",
			data: {transaction : id},
			success: function(data){
				data = JSON.parse(data);
				table.innerHTML = "";
				for(var i = 0; i < data.length; i++){
					entry = table.insertRow(-1);
					entry.insertCell(0).innerHTML = data[i][0];
					if(data[i][0] == 'Kerosene' || data[i][0] == 'Palm Oil')
						entry.insertCell(1).innerHTML = data[i][1] + ' Ltrs';
					else
						entry.insertCell(1).innerHTML = data[i][1] + ' Kgs';
				}
			}
		});
	}
  </script>	
</head>
<body>
<nav class="navbar navbar-inverse navbar-height">
	<div class="container-fluid">
    <div class="navbar-header text-center">
      <a class="navbar-brand" href="#"><font face="WildWest" size="6" color="#b37700">E-Ration</font></a>
    </div>
	<ul class="nav navbar-nav navbar-right">
	  <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-user"></span>
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
			<li><a href="#"><b>Hi, <?php echo $username; ?></b></a></li>
			<li class="divider"></li>
			<li><a href="user_home.php">Home</a></li>
			<li><a href="user_profile.php">Profile</a></li>
			<li><a href="transactions.php">Transactions</a></li>
			<li><a href="index.html">Logout</a></li>
		</ul>
      </li>
    </ul>
	</div>
</nav>

<div class="col-sm-1" style="width:100%">
	<h3 class="table-header" style="color:#484848"> Transactions </h3>
</div>
<div class="col-sm-1 pending" align="center">
<div class="adjust">
<table class="table" align="center">
    <thead>
      <tr>
        <th>ID</th>
        <th>Date</th>
		<th>Status</th>
      </tr>
    </thead>
    <tbody>
	<?php 
		$tsql = "SELECT * FROM TRANSACTIONS  WHERE UNAME='$username' ORDER BY ID DESC";
		$getResults = sqlsrv_query($conn, $tsql);
	
		while($row = sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC)){?>
			<tr>
				<td class="link" onclick='viewOrder(<?php echo $row['ID'] ?>);'><u><?php echo $row['ID'] ?></u></td>
				<td><?php echo date_format($row['DOT'], 'd M, Y'); ?></td>
				<td><?php echo $row['STATUS'] ?></td>
			</tr>
		<?php } ?>
    </tbody>
  </table>
</div>
</div>


<div class="modal fade" id="confirmation" role="dialog">
    <div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form>
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title" style="color:black">Order Preview</h4>
				</div>
				<div class="modal-body">
						<table class="table table-bordered" style="text-align:center">
						<thead>
						<tr>
							<th style="color:black; text-align:center">Product</th>
							<th style="color:black; text-align:center">Quantity</th>
						</tr>
						</thead>
						<tbody id="order" style="color:black">
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

</body>
</html>
