<?php
    session_start();
    $username = $_SESSION['USERNAME'];
?>


<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<br>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">e-Ration</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
      <li><a href="#">Page 1</a></li>
      <li><a href="#">Page 2</a></li>
	</ul>
	<ul class="nav navbar-nav navbar-right">
	  <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-user"></span>
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
			<li><a href="#"><?php echo $username; ?></a></li>
			<li class="divider"></li>
			<li><a href="#">Profile</a></li>
			<li><a href="#">Transaction</a></li>
			<li><a href="#">Update</a></li>
		</ul>
      </li>
    </ul>
  </div>
</nav>

<br>
<div>
<table class="table table-striped">
    <thead>
      <tr>
        <th>Customer Name</th>
        <th>Item</th>
        <th>Date</th>
		<th>Status</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Saravanan</td>
        <td>Rice</td>
        <td>14/2/18</td>
		<td><span class="badge">Pending</span></td>
      </tr>
      <tr>
        <td>JD</td>
        <td>Wheat</td>
        <td>14/2/18</td>
		<td><span class="badge">Pending</span></td>
      </tr>
      <tr>
        <td>Rathna Subramanian</td>
        <td>Oil</td>
        <td>15/2/18</td>
		<td><span class="badge" style="background-color:green" >Done</span></td>
      </tr>
    </tbody>
  </table>
</div>

</body>
</html>
