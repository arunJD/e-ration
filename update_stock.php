<?php
    session_start();
    $username = $_SESSION['USERNAME'];
	$shop = $_SESSION['SHOP_ID'];
?>
<html>
<head>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="update_stock.css">
  <script>
    var data= [
		['Rice', 0],
		['Wheat', 0],
		['Sugar', 0],
		['Kerosene', 0],
		['Toor Dal', 0],
		['Urad Dal', 0],
		['Palm Oil', 0],
	];
	function confirmation(){
		data[0][1] = parseInt($('#rice').val());
		data[1][1] = parseInt($('#sugar').val());
		data[2][1] = parseInt($('#wheat').val());
		data[3][1] = parseInt($('#kerosene').val());
		data[4][1] = parseInt($('#toor').val());
		data[5][1] = parseInt($('#urad').val());
		data[6][1] = parseInt($('#palm').val());
		console.log(data);
		var count = 0;
		for(var i = 0; i < 7; i++){
			if(data[i][1] > 0){
				count++;
			}
			else{
				data[i][1] = 0;
			}
		}
		console.log(data);
		if(count > 0){
			var myModal = $('#confirmUpdate');
			myModal.modal({ show: true });
			var table = document.getElementById('order');
			var entry;
			table.innerHTML = "";
			for(var i = 0; i < 7; i++){
				if(data[i][1] > 0){
					entry = table.insertRow(-1);
					entry.insertCell(0).innerHTML = data[i][0];
					if(data[i][0] == 'Kerosene' || data[i][0] == 'Palm Oil')
						entry.insertCell(1).innerHTML = data[i][1] + ' Ltrs';
					else
						entry.insertCell(1).innerHTML = data[i][1] + ' Kgs';
				}
			}
		}
	}
	function update(){
		var id = "<?php echo $shop ?>";
		$.ajax({
			type: "GET",
			url: "update.php",
			data: {shop: id, rice: data[0][1], sugar: data[1][1], wheat: data[2][1], kerosene: data[3][1], toor: data[4][1], urad: data[5][1], palm: data[6][1]},
			success: function(data){
				console.log('success');
			}
		});
	}
  </script>
</head>
<body>
<nav class="navbar navbar-inverse navbar-height">
	<div class="container-fluid">
    <div class="navbar-header text-center">
      <a class="navbar-brand" href="#"><font face="WildWest" size="6" color="#b37700">E-Ration</font></a>
    </div>
	<ul class="nav navbar-nav navbar-right">
	  <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-user"></span>
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
			<li><a href="#"><b>Hi, <?php echo $username; ?></b></a></li>
			<li class="divider"></li>
			<li><a href="vendor.php">Home</a></li>
			<li><a href="vendor_profile.php">Profile</a></li>
			<li><a href="update_stock.php">Update Stock</a></li>
			<li><a href="index.html">Logout</a></li>
		</ul>
      </li>
    </ul>
	</div>
</nav>

<div class="update">
	<ul style="list-style:none;">
		<li class="list">
			Rice
			<input type="number" step="1" align="right" id="rice" class="items" min="0"> (in Kg)
		</li>
		<li class="list">
			Sugar
			<input type="number" step="1" align="right" id="sugar" class="items" min="0"> (in Kg)
		</li>
		<li class="list">
			Wheat
			<input type="number" step="1" align="right" id="wheat" class="items" min="0"> (in Kg)
		</li>
		<li class="list">
			Kerosene
			<input type="number" step="1" align="right" id="kerosene" class="items" min="0"> (in Ltr)
		</li>
		<li class="list">
			Toor Dal
			<input type="number" step="1" align="right" id="toor" class="items" min="0"> (in Kg)
		</li>
		<li class="list">
			Urad Dal
			<input type="number" step="1" align="right" id="urad" class="items" min="0"> (in Kg)
		</li>
		<li class="list">
			Palm Oil
			<input type="number" step="1" align="right" id="palm" class="items" min="0"> (in Ltr)
		</li>
	</ul>
	<p align="center">
	<button class="btn btn-primary" style="height:40px; width:120px; font-size:16px" onclick="confirmation()">Update</button>
	</p>
</div>

<div class="modal fade" id="confirmUpdate" role="dialog">
    <div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form>
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal">&times;</button>
				  <h4 class="modal-title" style="color:black">Confirm Update</h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered" style="text-align:center">
						<thead>
						<tr>
							<th style="color:black; text-align:center">Product</th>
							<th style="color:black; text-align:center">Quantity</th>
						</tr>
						</thead>
						<tbody id="order" style="color:black">
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-primary" data-dismiss="modal" onclick='update()'>Confirm</button>
				  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>